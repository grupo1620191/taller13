#include <unistd.h>
#include <string.h>
#include "objetos.h"
#include <stdlib.h>

void crear_objeto_ejemplo(void *ref, size_t tamano){
	objeto_ejemplo *oe = (objeto_ejemplo *) ref;
	oe->id_hilo = 0;
	oe->data = malloc(3*sizeof(int));
}

void destruir_objeto_ejemplo(void *ref, size_t tamano){
	objeto_ejemplo *oe = (objeto_ejemplo *) ref;
	oe->id_hilo = 0;
	oe->data = 0;
	free(oe->data);
}
