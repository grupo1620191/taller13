#include "../include/slaballoc.h"
#include "../include/objetos.h"
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

void* proceso_hilo(void* args){
	SlabAlloc *slaballoc = (SlabAlloc*) args;
	objeto_ejemplo objetos[1000];

	for(int k = 0; k < 1000; k++){
	  objeto_ejemplo *obj = (void *)obtener_cache(slaballoc);
	  obj->id_hilo = k;
	  obj->data[0] = k*0;
	  obj->data[1] = k*1;
	  obj->data[2] = k*2;
	  objetos[k] = *obj;
	}

	for (int l = 700; l < 900; l++) {
	  devolver_cache(slaballoc, &objetos[l]);
	}

	for (int m = 0; m < 700; m++) {
	  if (objetos[m].id_hilo != m){
	    printf("ERROR. Valor esperado: id: %d\n Valor encontrado: id: %d\n",m, objetos[m].id_hilo);
	  }
	  if (objetos[m].data[0] != m*0){
	    printf("ERROR. Valor esperado: data[0]: %d\n Valor encontrado: data[0]: %d\n",m*0, objetos[m].data[0]);
	  }
	  if (objetos[m].data[1] != m*1){
	    printf("ERROR. Valor esperado: data[1]: %d\n Valor encontrado: data[1]: %d\n",m*1, objetos[m].data[1]);
	  }
	  if (objetos[m].data[2] != m*2){
	    printf("ERROR. Valor esperado: data[2]: %d\n Valor encontrado: data[2]: %d\n\n",m*2, objetos[m].data[2]);
	  }
	}
	return NULL;
}

int main(int argc, char **argv){
	if (argc != 3 || strcmp(argv[1],"-n")) {
		printf("ejecutable -n numeroHilos\n");
		exit(1);
	}

	int nHilos = atoi(argv[2]);
	pthread_t p[nHilos];

	SlabAlloc *slabAlloc1;

	constructor_fn constructor = crear_objeto_ejemplo;
	destructor_fn destructor = destruir_objeto_ejemplo;

	slabAlloc1 = crear_cache("SLABALLOC", sizeof(objeto_ejemplo), constructor, destructor);

	memset(p,0,nHilos*sizeof(pthread_t));

	for (int i = 0; i < nHilos; i++) {
		pthread_t id;
		int nh = pthread_create(&id, NULL, proceso_hilo, (void *)slabAlloc1);
		if (nh >= 0) {
			p[i] = id;
		}
	}

	for (int j = 0; j < nHilos; j++) {
		pthread_join(p[j], NULL);
	}

}
